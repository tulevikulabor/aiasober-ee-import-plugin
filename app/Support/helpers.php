<?php

if (!function_exists('printr')) {
    function printr($variable)
    {
        echo '<pre>';
        print_r($variable);
        echo '</pre>';
    }
}
