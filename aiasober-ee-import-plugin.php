<?php

/**
 * Plugin Name:         Aiasõber Tulevikulabor Import Plugin
 * Plugin URI:          https://gitlab.com/tulevikulabor/aiasober-ee-import-plugin
 * Description:         Aiasõber Tulevikulabor Import/Export porduct plugin.
 * Version:             1.0
 * Requires at least:   5.5
 * Requires PHP:        7.3
 * Author:              Raido Orumets <raido.orumets@gmail.com>
 * Author URI:          https://orumets.ee/
 * License:             Aiasõber
 * License URI:         https://aiasober.ee
 * Text Domain:         aiasober-ee-import-plugin
 * Domain Path:         /languages
 */

if (! defined('AIASOBER_EE_IMPORT_PLUGIN')) {
    define('AIASOBER_EE_IMPORT_PLUGIN', '1.0');
}

define('PLUGIN_PATH', plugin_dir_path(__FILE__) . '/');

require_once __DIR__ . '/app/includes.php';

global $sitepress;
$sitepress->switch_lang('et');

function my_get_all_products($data)
{
    $args = [
        'post_type' => 'product',
        //'posts_per_page' => 100,
        'limit' => -1,
        'lang' => 'et',
    ];
    
    $products = wc_get_products($args);
    
    return ($products) ? $products : [];
}

add_action('rest_api_init', function () {
    register_rest_route('aiasober/import/v1', '/products', [
        'methods' => 'GET',
        'callback' => 'my_get_all_products',
    ]);
});


/*

// An array of all published WC_Product Objects
$products = wc_get_products( array( 'status' => 'publish', 'limit' => -1 ) );

// Displaying the number of products in this array
echo '<p>Number of products: ' . sizeof( $products ) . '</p>';

// Loop through products and display some data using WC_Product methods
foreach ( $products as $product ){
    echo '<p>';
    echo 'Type: '  . $product->get_type() . '<br>';  // Product type
    echo 'ID: '    . $product->get_id() . '<br>';    // Product ID
    echo 'Title: ' . $product->get_title() . '<br>'; // Product title
    echo 'Price: ' . $product->get_price();          // Product price
    echo '</p>';
}
 */
